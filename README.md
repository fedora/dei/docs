Fedora DEI Team documentation
=============================

<!--
    Style rule: one sentence per line please!
    This makes git diffs easier to read. :)
-->

[![License: CC BY-SA 4.0][1]](https://creativecommons.org/licenses/by-sa/4.0/ "Fedora Docs content licensed under CC BY-SA 4.0")

This repository contains the source content for the [Fedora DEI Team documentation][2].
The Antora configuration files and AsciiDoc source content are hosted in this repository.
Please submit Merge Requests for content fixes here.


## Report an issue with the DEI Team docs

Noticed some wrong or incorrect in the [Fedora DEI Team documentation][2]?
Open a new Issue on the [DEI Home repository][3] to report the problem.
Check the [list of open issues][4] first to see if someone else has already reported the problem before opening a new issue.

### [Fedora DEI Team docs issues][4]


## How to contribute to DEI Team docs

See [CONTRIBUTING.md][5].


## How to contact the DEI Team

The Fedora DEI Team uses the following communication platforms:

* _Asynchronous_:
  [discussion.fedoraproject.org/tag/dei-team](https://discussion.fedoraproject.org/tag/dei-team)
* _Synchronous_:
  [matrix.to/#/#dei:fedoraproject.org](https://matrix.to/#/#dei:fedoraproject.org)

The Discourse forum is best for _asynchronous_ communication.
This means it is best for questions or topics that someone may respond to later. It is better for longer, threaded discussions.

The Matrix room is best for _synchronous_ communication.
This means it is best for quick feedback, like a conversation.
It is helpful for real-time discussions or getting someone's attention more quickly.

[1]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
[2]: https://docs.fedoraproject.org/en-US/dei/
[3]: https://gitlab.com/fedora/dei/home
[4]: https://gitlab.com/fedora/dei/home/-/issues/?label_name%5B%5D=category%3A%3Adocs
[5]: https://gitlab.com/fedora/dei/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads
