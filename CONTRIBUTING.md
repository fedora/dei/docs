How to contribute to Fedora DEI Team documentation
==================================================

<!--
    Style rule: one sentence per line please!
    This makes git diffs easier to read. :)
-->

Please report issues and submit pull requests for **Content Fixes** to [Fedora DEI documentation][1] here.
General appearance issues and publishing issues should be reported against the [publishing software][2].

Never done a pull request (or "PR")?
See the [GitHub Guides](https://guides.github.com/) for getting started with git.
If you already know git, see the [GitLab documentation for Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/) on how to submit contributions on GitLab.


## How to edit these documents

Content for the Fedora DEI Team documentation is stored [in this repository](https://gitlab.com/fedora/dei/docs/-/tree/main/modules?ref_type=heads).
Source content is written in [AsciiDoc](http://asciidoc.org/).
See these resources for help using AsciiDoc:

* [AsciiDoc Syntax - Quick Reference](http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)
* [AsciiDoc Recommended Practices](https://asciidoctor.org/docs/asciidoc-recommended-practices/)

<!--

Advanced topics for AsciiDoc:

* [AsciiDoc Writer's Guide](http://asciidoctor.org/docs/asciidoc-writers-guide/)
* [Antora Documentation](https://docs.antora.org/antora/1.0/page/)

-->


## Test with a local preview

This repo includes scripts to build and preview the contents of this repository.

**NOTE**:
If you use a `xref` to link to pages in other repositories, those links will not work in the local preview, as it only builds this repository.
If you want to rebuild the whole Fedora Docs site, see the [Fedora Docs build repository][2] for more instructions.

Both of the below scripts use containers, so ensure either Podman or Docker is installed on your system before beginning.
To build and preview the site, open a command prompt or terminal and execute this command:

```bash
./build.sh && ./preview.sh
```

The local preview is visible at [http://localhost:8080](http://localhost:8080).

[1]: https://docs.fedoraproject.org/en-US/dei/
[2]: https://gitlab.com/fedora/docs/docs-website/docs-fp-o
