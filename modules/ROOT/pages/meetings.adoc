include::ROOT:partial$attributes.adoc[]

= Team meeting guide

This guide contains instructions and recommendations for running {team_name} meetings.


[[social-agreement]]
== Social agreement

The social agreement is our social rules and etiquette for how we treat each other during meetings.
This keeps our meetings productive and gives everyone an opportunity to be heard:

* Focus on listening
* Stay present as much as possible, refrain from email/chat/etc
* Let everyone have a chance to speak, be aware of how much time you are talking
* Be mindful of the meeting schedule and help to move the dialog forward
* Use the raise hand function
* Stay on mute when you are not speaking


[[format]]
== Format

Since 24 October 2020, the {team_name} adopted this format for structuring team meeting agendas:

. Intros, welcomes, hellos
  (5 minutes)
. Team announcements & news
  (5-10 minutes)
. Follow-ups from last meeting, if any
  (5-10 minutes)
. Ticket discussions
  (remaining time)
. Open floor
  (final 5-10 minutes)


[[add-ticket]]
== How to add a ticket to meeting agenda

Add a GitLab issue to the next meeting agenda by adding them to a *milestone* for the current quarter of the year.
First, open a GitLab issue and look in the metadata sidebar to the right.
Then, click _Edit_ in the _Milestone_ section in the sidebar.
Several options should appear in the dropdown menu that appears, e.g. _2024 Q4 Team Goals_.
Choose the milestone that corresponds to the current quarter.
Once selected, the GitLab issue will appear under the milestone.
The {team_name} uses GitLab milestones to triage discussions for our meetings.

image::meetings/gitlab-update-issue-milestone.png[Screenshot of how to update the milestone of a GitLab issue]


[[meetbot]]
== Meetbot command script

Since late 2023, Fedora uses *Meetbot* for running text-based meetings from our Matrix meeting rooms, e.g. `#meeting:fedoraproject.org`
Meetbot recognizes several commands to start a logged meeting that will generate a summary once the meeting ends.
This section includes a sample script for a Meetbot-powered meeting on Fedora Chat.

----
!startmeeting Fedora DEI Team - YYYY-MM-DD
!meetingname diversity


!topic Agenda

!info (1) Intros, welcomes, hellos
!info (2) Team announcements & news
!info (3) DEI Advisor updates to the team
!info (4) Follow-ups from last meeting
!info (5) Ticket-driven discussion
!info (6) Open floor


!topic Intros, welcomes, hellos

If this is your first time at a DEI Team meeting, please say hello!
If you have questions before we start the meeting, now is also a good time to ask.

!info Present: fas-id1, fas-id2, fas-id3


!topic Team announcements & news

!info === My announcement here ===
!link https://example.com/my-announcement/


!topic DEI Advisor updates to the team

!info === My announcement here ===
!link https://example.com/my-announcement/


!topic Follow-ups from last meeting

!info How This Works: We review past action items from the last meeting. For each action item, we note a quick update on its status and then move forward.

!info === [assignee] action item here ===
!link https://example.com/my-update/


!topic Ticket-driven discussion

!link https://gitlab.com/fedora/dei/home/-/milestones/XY


!topic Ticket #XY: "ticket title"

!link https://gitlab.com/fedora/dei/home/-/issues/XY
!info [ any updates to share for meeting discussion ]


!topic Open floor

!endmeeting
----

[[meetbot-commands]]
=== Meetbot commands

Curious what else Meetbot is capable of?
Below are the full commands you can use with Meetbot after a meeting has started.

* `!meetingname <a new name>`:
  Rename the meeting.
* `!topic <a topic name>`:
  Change the agenda topic of the meeting.
* `!endmeeting`:
  End the meeting and generate the meeting summary.

There are also several handy tags that you can use to tag a message to highlight it in the minutes (and add an emoji reaction here):

* 🚩 `!action`:
  An action item that needs follow-up.
  All action items will be listed separately in the meeting summary generated after the meeting ends.
* ✏️ `!info`:
  Information or details that summarize the discussion or context about the topic.
* 👍 `!agreed`:
  A statement that the team has agreed to as a final decision.
* 👎 `!rejected`:
  A statement that the team has rejected as an outcome or a decision.
* 💭 `!idea`:
  A new idea or approach to a challenge or opportunity.
* 🛟 `!halp`:
  Help!
  Call attention for action items or work that needs help from other team members.
* 🔗 `!link`:
  URLs to additional reading material or deliverables related to the topic.
